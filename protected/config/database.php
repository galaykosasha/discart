<?php

// This is the database connection configuration.
return array(
	'connectionString' => 'mysql:host=localhost;dbname=discart',
    'emulatePrepare' => true,
    'enableProfiling' => false,
    'enableParamLogging' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
    'tablePrefix' => 'dsc_',
);