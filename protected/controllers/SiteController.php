<?php

class SiteController extends Controller
{
    public function actionExcel()
    {
        $model = new Carts('search');
        $model->unsetAttributes();
        if (isset($_GET['Model'])) {
            $model->attributes = $_GET['Model'];
        }
        if (isset($_GET['export'])) {
            $production = 'export';
        } else {
            $production = 'grid';
        }
        $this->render('excel', array('model' => $model, 'production' => $production));
    }


    /**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
       /* if(isset($_GET['id'])) {
            $this->AddCarts($_GET['id']);
            Yii::app()->end();
        }

        $carts = Carts::model()->findAll(array('select'=>'cart_number', 'order'=>'id ASC', 'limit'=>10000));
        foreach($carts as $cart) {
            echo ''.$cart->cart_number.''."\r\n".'<br>';
        }*/
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

    public function AddCarts($id)
    {
        $model = new Carts();
        $model->cart_number = $this->newNumber($this->getIssetsNumbers());
        if($model->save()) {
            echo $id;
        }
    }

    private function getIssetsNumbers(){
        $array = array();
        $numbersCarts = Carts::model()->findAll(array('select'=>'cart_number'));
        foreach($numbersCarts as $cart){
            $array[] = $cart->cart_number;
        }
        return $array;
    }

    private function newNumber($array)
    {
        $number = $this->generateNumber();
        if(!in_array($number, $array)) {
            return $number;
        } else {
            $this->newNumber($array);
        }
    }

    private function generateNumber()
    {
        $str = '';
        for($i = 0; $i < 16; $i++)
        {
            $str .= rand(0, 9);
        }
        return $str;
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}