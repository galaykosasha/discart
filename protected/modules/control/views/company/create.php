<?php
/* @var $this CompanyController */
/* @var $company Company */
/* @var $companyDescription CompanyDescription */
/* @var $companyAddress CompanyAddress */
$this->actionHeader = Yii::t('main', 'Добавление').' '.'Company';
$this->breadcrumbs=array(
	'Companies'=>array('index'),
	Yii::t('main', 'Добавление'),
);
?>
<?php $this->renderPartial('_form', array(
    'company' => $company,
    'companyDescription' => $companyDescription,
    'companyAddress' => $companyAddress,
)); ?>