<?php
/* @var $this CompanyController */
/* @var $company Company */
/* @var $companyDescription CompanyDescription */
/* @var $companyAddress CompanyAddress */
$this->actionHeader = Yii::t('main', 'Редактирование').' '.'Company'.' '.$company->id;
$this->breadcrumbs=array(
	'Companies'=>array('index'),
	Yii::t('main', 'Редактирование'),
);
?>

<?php $this->renderPartial('_form', array(
    'company' => $company,
    'companyDescription' => $companyDescription,
    'companyAddress' => $companyAddress,
)); ?>