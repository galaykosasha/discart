<?php
/* @var $this CompanyController */
/* @var $company Company */
/* @var $companyDescription CompanyDescription */
/* @var $companyAddress CompanyAddress */
/* @var $form CActiveForm */
?>
<div class="row">
    <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'company-form',
                'enableAjaxValidation'=>false,
            )); ?>
    <div class="col-xs-12">
        <!---- Flash message ---->
         <?php $this->beginWidget('application.modules.control.components.widgets.FlashWidget',array(
            'params'=>array(
                'model' => $company,
                'form' => null,
            )));
        $this->endWidget(); ?>
        <!---- End Flash message ---->
    </div>

    <div class="col-md-6">
        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Main'); ?>
                </h3>
            </div>
            <div class="box-body">
                	            
                <div class="form-group">
                    <?= $form->labelEx($company,'password'); ?>
                    <?= $form->passwordField($company,'password',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($company,'password'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($company,'telephone'); ?>
                    <?= $form->textField($company,'telephone',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($company,'telephone'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($company,'type'); ?>
                    <?= $form->dropDownList($company,'type', CHtml::listData(CompanyType::model()->findAll(), 'id', 'type_name'), array('class'=>'form-control', 'empty'=>_t('Select Company Type'))); ?>
                    <?= $form->error($company,'type'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($company,'segment'); ?>
                    <?= $form->textField($company,'segment', array('class'=>'form-control')); ?>
                    <?= $form->error($company,'segment'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($company,'company_chain_id'); ?>
                    <?= $form->dropDownList(
                        $company,
                        'company_chain_id',
                        CHtml::listData(CompanyChains::model()->findAll(), 'id', 'name'),
                        array(
                            'class'=>'form-control',
                            'empty'=>_t('Select {attribute}',array('{attribute}'=>$company->getAttributeLabel('company_chain_id')))
                        )
                    ); ?>
                    <?= $form->error($company,'company_chain_id'); ?>
                </div>
            </div>

            <div class="box-footer">
                <?php echo CHtml::submitButton($company->isNewRecord ? Yii::t('main', 'Добавить') : Yii::t('main', 'Сохранить'), array('class'=>'btn btn-primary')); ?>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Description'); ?>
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <?= $form->labelEx($companyDescription,'name_en'); ?>
                    <?= $form->textField($companyDescription,'name_en',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($companyDescription,'name_en'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($companyDescription,'name_ru'); ?>
                    <?= $form->textField($companyDescription,'name_ru',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($companyDescription,'name_ru'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($companyDescription,'name_uk'); ?>
                    <?= $form->textField($companyDescription,'name_uk',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($companyDescription,'name_uk'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($companyDescription,'description_en'); ?>
                    <?= $form->textArea($companyDescription,'description_en',array('rows'=>3, 'cols'=>50, 'class'=>'form-control')); ?>
                    <?= $form->error($companyDescription,'description_en'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($companyDescription,'description_ru'); ?>
                    <?= $form->textArea($companyDescription,'description_ru',array('rows'=>3, 'cols'=>50, 'class'=>'form-control')); ?>
                    <?= $form->error($companyDescription,'description_ru'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($companyDescription,'description_uk'); ?>
                    <?= $form->textArea($companyDescription,'description_uk',array('rows'=>3, 'cols'=>50, 'class'=>'form-control')); ?>
                    <?= $form->error($companyDescription,'description_uk'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($companyDescription,'phones'); ?>
                    <?= $form->textField($companyDescription,'phones',array('size'=>60, 'maxlength'=>255, 'class'=>'form-control')); ?>
                    <?= $form->error($companyDescription,'phones'); ?>
                </div>
            </div>
        </div>
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Address'); ?>
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <?= $form->labelEx($companyAddress,'country'); ?>
                    <?= $form->textField($companyAddress,'country',array('size'=>60, 'maxlength'=>100, 'class'=>'form-control')); ?>
                    <?= $form->error($companyAddress,'country'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($companyAddress,'city'); ?>
                    <?= $form->textField($companyAddress,'city',array('size'=>60, 'maxlength'=>100, 'class'=>'form-control')); ?>
                    <?= $form->error($companyAddress,'city'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($companyAddress,'street'); ?>
                    <?= $form->textField($companyAddress,'street',array('size'=>60, 'maxlength'=>100, 'class'=>'form-control')); ?>
                    <?= $form->error($companyAddress,'street'); ?>
                </div>


                <div class="form-group">
                    <?= $form->labelEx($companyAddress,'house'); ?>
                    <?= $form->textField($companyAddress,'house',array('size'=>60, 'maxlength'=>100, 'class'=>'form-control')); ?>
                    <?= $form->error($companyAddress,'house'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>