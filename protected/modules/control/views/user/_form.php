<?php
/* @var $this UserController */
/* @var $model User */
/* @var $card Cards */
/* @var $form CActiveForm */
?>
<div class="row">
    <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'user-form',
                'enableAjaxValidation'=>false,
            )); ?>
    <div class="col-xs-12">
        <!---- Flash message ---->
         <?php $this->beginWidget('application.modules.control.components.widgets.FlashWidget',array(
            'params'=>array(
                'model' => $model,
                'form' => null,
            )));
        $this->endWidget(); ?>
        <!---- End Flash message ---->
    </div>

    <div class="col-md-6">
        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Основные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">
                	            
                <div class="form-group">
                    <?= $form->labelEx($model,'telephone'); ?>
                    <?= $form->textField($model,'telephone',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'telephone'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model,'card_id'); ?>
                    <?= $form->hiddenField($model, 'card_id', array('value'=>$card->id)); ?>
                    <?= $form->textField($card, 'card_number', array('disabled'=>true, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'card_id'); ?>
                </div>
            </div>

            <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Добавить') : Yii::t('main', 'Сохранить'), array('class'=>'btn btn-primary')); ?>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Дополнительные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <?= $form->labelEx($model,'sex'); ?>
                    <?= $form->dropDownList($model,'sex', array($model::SEX_FEMALE => Yii::t('main', 'Female'), $model::SEX_MALE=>Yii::t('main', 'Male')), array('class'=>'form-control', 'empty'=>Yii::t('main', 'Select sex'))); ?>
                    <?= $form->error($model,'sex'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>