<?php
/* @var $this UserCompaniesController */
/* @var $model UserCompanies */
$this->actionHeader = Yii::t('main', 'Редактирование').' '.'UserCompanies'.' '.$model->id;
$this->breadcrumbs=array(
	'User Companies'=>array('index'),
	Yii::t('main', 'Редактирование'),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>