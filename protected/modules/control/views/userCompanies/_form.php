<?php
/* @var $this UserCompaniesController */
/* @var $model UserCompanies */
/* @var $form CActiveForm */
?>
<div class="row">
    <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'user-companies-form',
                'enableAjaxValidation'=>false,
            )); ?>
    <div class="col-xs-12">
        <!---- Flash message ---->
         <?php $this->beginWidget('application.modules.control.components.widgets.FlashWidget',array(
            'params'=>array(
                'model' => $model,
                'form' => null,
            )));
        $this->endWidget(); ?>
        <!---- End Flash message ---->
    </div>

    <div class="col-md-6">
        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Основные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">

                <div class="form-group">
                    <?= $form->labelEx($model,'discount'); ?>
                    <div class="input-group">
                        <?= $form->numberField($model,'discount',array('size'=>2, 'maxlength'=>2, 'class'=>'form-control')); ?>
                        <span class="input-group-addon">%</span>
                    </div>
                    <?= $form->error($model,'discount'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'user_id'); ?>
                    <?= $form->dropDownList($model,'user_id', CHtml::listData(User::model()->findAll(), 'id', 'telephone'), array('class'=>'form-control', 'empty'=>_t('Select User by telephone'))); ?>
                    <?= $form->error($model,'user_id'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'company_id'); ?>
                    <?= $form->dropDownList($model,'company_id', CHtml::listData(Company::model()->findAll(), 'id', 'name_ru'), array('class'=>'form-control', 'empty'=>_t('Select Company'))); ?>
                    <?= $form->error($model,'company_id'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'company_chain_id'); ?>
                    <?= $form->dropDownList($model,'company_chain_id', CHtml::listData(CompanyChains::model()->findAll(), 'id', 'name'), array('class'=>'form-control', 'empty'=>_t('Select Company Chain'))); ?>
                    <?= $form->error($model,'company_chain_id'); ?>
                </div>

                            
            </div>

            <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Добавить') : Yii::t('main', 'Сохранить'), array('class'=>'btn btn-primary')); ?>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Дополнительные настройки'); ?>
                </h3>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>