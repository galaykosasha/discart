<?php
/* @var $this UserCompaniesController */
/* @var $model UserCompanies */
$this->actionHeader = Yii::t('main', 'Добавление').' '.'UserCompanies';
$this->breadcrumbs=array(
	'User Companies'=>array('index'),
	Yii::t('main', 'Добавление'),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>