<?php
/* @var $this CompanyTypeController */
/* @var $model CompanyType */
$this->actionHeader = Yii::t('main', 'Добавление').' '.'CompanyType';
$this->breadcrumbs=array(
	'Company Types'=>array('index'),
	Yii::t('main', 'Добавление'),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>