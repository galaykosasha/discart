<?php
/* @var $this CompanyTypeController */
/* @var $model CompanyType */
$this->actionHeader = Yii::t('main', 'Редактирование').' '.'CompanyType'.' '.$model->id;
$this->breadcrumbs=array(
	'Company Types'=>array('index'),
	Yii::t('main', 'Редактирование'),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>