<?php
/* @var $this CompanyChainsController */
/* @var $model CompanyChains */
$this->actionHeader = Yii::t('main', 'Редактирование').' '.'CompanyChains'.' '.$model->id;
$this->breadcrumbs=array(
	'Company Chains'=>array('index'),
	Yii::t('main', 'Редактирование'),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>