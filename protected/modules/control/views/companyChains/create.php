<?php
/* @var $this CompanyChainsController */
/* @var $model CompanyChains */
$this->actionHeader = Yii::t('main', 'Добавление').' '.'CompanyChains';
$this->breadcrumbs=array(
	'Company Chains'=>array('index'),
	Yii::t('main', 'Добавление'),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>