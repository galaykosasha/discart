<?php
/* @var $this CardsController */
/* @var $model Cards */
/* @var $form CActiveForm */
?>
<div class="row">
    <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'cards-form',
                'enableAjaxValidation'=>false,
            )); ?>
    <div class="col-xs-12">
        <!---- Flash message ---->
         <?php $this->beginWidget('application.modules.control.components.widgets.FlashWidget',array(
            'params'=>array(
                'model' => $model,
                'form' => null,
            )));
        $this->endWidget(); ?>
        <!---- End Flash message ---->
    </div>

    <div class="col-md-6">
        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Основные настройки'); ?>
                </h3>
            </div>
            <div class="box-body">
                	            
                <div class="form-group">
                    <?= $form->labelEx($model,'card_number'); ?>
                    <?= $form->textField($model,'card_number',array('size'=>16, 'maxlength'=>16, 'class'=>'form-control')); ?>
                    <?= $form->error($model,'card_number'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'active'); ?>
                    <?= $form->textField($model, 'active', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'active'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'status'); ?>
                    <?= $form->textField($model, 'status', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'status'); ?>
                </div>

            
                <div class="form-group">
                    <?= $form->labelEx($model,'company_id'); ?>
                    <?= $form->textField($model, 'company_id', array('class'=>'form-control')); ?>
                    <?= $form->error($model,'company_id'); ?>
                </div>

                            
            </div>

            <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Добавить') : Yii::t('main', 'Сохранить'), array('class'=>'btn btn-primary')); ?>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= Yii::t('main', 'Дополнительные настройки'); ?>
                </h3>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>