<?php
/* @var $this CardsController */
/* @var $model Cards */
$this->actionHeader = Yii::t('main', 'Добавление').' '.'Cards';
$this->breadcrumbs=array(
	'Cards'=>array('index'),
	Yii::t('main', 'Добавление'),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>