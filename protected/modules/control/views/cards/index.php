<?php
/* @var $this CardsController */
/* @var $model Cards */

$this->actionHeader = Yii::t('main', 'Управление').' '.'Cards';
$this->breadcrumbs=array(
	'Cards'=>array('index'),
	Yii::t('main', 'Управление'),
);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h5 class="box-title">
                    Cards
                </h5>
                <div class="button_save">
                    <?= CHtml::link('<i class="fa fa-plus"></i>&nbsp;'.Yii::t('main', 'Добавить'), array('/control/cards/create'), array('class'=>'pull-right btn btn-info btn-flat')); ?>
                    <?= CHtml::tag('span', array('class'=>'pull-right btn btn-info btn-flat', 'data-toggle'=>'modal', 'data-target'=>'#myModal'), '<i class="fa fa-plus"></i> &nbsp;'.Yii::t('main', 'Генерация карт')); ?>
                </div>
            </div>
            <div class="box-body">
                <?php $this->widget('application.modules.control.components.widgets.overWrite.AdminGridView', array(
                'id'=>'cards-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
                    array(
                        'header'=>'',
                        'value'=>'CHtml::link("<i class=\"fa fa-user-plus\"></i>", array("/control/user/create", "card_id"=>$data->id))',
                        'type'=>'html',
                    ),
                    'id',
                    'card_number',
                    'active',
                    'status',
                    'company_id',
                    array(
                        'class'=>'CButtonColumn',
                        'htmlOptions' => array('style'=>'text-align: center; width: 80px;'),
                        'template'=>'{update}&nbsp;{delete}',
                        'buttons' => array(
                            'update' => array(
                                'imageUrl'=>$this->assetsPath.'/images/edit.png',
                            ),
                            'delete' => array(
                                'imageUrl'=>$this->assetsPath.'/images/delete.png',
                            ),
                        ),
                    ),
                ),
                )); ?>
            </div>
        </div>
    </div>
</div>


<div class="modal modal-info fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #fff;">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Modal Info</h4>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <input class="form-control" type="text">
                    <span class="input-group-addon">
                        <i class="fa fa-check"></i>
                    </span>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-outline pull-left" type="button">Close</button>
                <button class="btn btn-outline" type="button">Генерировать</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
 <!--
<script>
    for(i = 0; i < 730; i++){
        jQuery.ajax({
            url: "/site/index?id="+i,
            async: false,
            success: function(v){
                $('#done').html(v);
            }
        });
    }
</script>-->