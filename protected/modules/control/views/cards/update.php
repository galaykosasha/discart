<?php
/* @var $this CardsController */
/* @var $model Cards */
$this->actionHeader = Yii::t('main', 'Редактирование').' '.'Cards'.' '.$model->id;
$this->breadcrumbs=array(
	'Cards'=>array('index'),
	Yii::t('main', 'Редактирование'),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>