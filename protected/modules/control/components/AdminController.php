<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AdminController extends CController
{
    public $layout='/layouts/admin';
    public $actionHeader = '';
    public $mainMenu = '';
    public $breadcrumbs=array();
    private $_assetsPath;

    public function init()
    {
        function _t($message, $params=array(),$source=null,$language=null, $category = 'controlModule.main')
        {
            return Yii::t($category, $message, $params,$source,$language);
        }
        $this->getMainMenu();
    }

    private function getMainMenu()
    {
        $this->mainMenu = array(
            array(
                'label'=>'
                <i class="fa fa-home"></i>
                <span>Главная</span>
            ',
                'url'=>array('/control/default/index'),
                'itemOptions'=>array('class'=>'treeview'),
            ),
            array(
                'label'=>'
                <i class="fa fa-file-text-o"></i>
                <span>'._t('Cards').'</span>
            ',
                'url'=>array('/control/cards/index'),
                'itemOptions'=>array('class'=>'treeview'),
            ),

            array(
                'label'=>'
                <i class="fa fa-file-text-o"></i>
                <span>'._t('Companies').'</span>
                <i class="fa fa-angle-left pull-right"></i>
                ',
                'url'=>array('#'),
                'itemOptions'=>array('class'=>'treeview'),
                'items'=>array(
                    array(
                        'label'=>'
                        <i class="fa fa-circle-o"></i>
                        <span>'._t('Company chains').'</span>
                        ',
                            'url'=>array('/control/companyChains/index'),
                        ),
                    array(
                        'label'=>'
                            <i class="fa fa-circle-o"></i>
                            <span>'._t('Company types').'</span>
                            ',
                            'url'=>array('/control/companyType/index'),
                        ),
                    array(
                        'label'=>'
                        <i class="fa fa-circle-o"></i>
                        <span>'._t('Companies').'</span>
                        ',
                            'url'=>array('/control/company/index'),
                    ),
                ),
            ),
            array(
                'label'=>'
                <i class="fa fa-file-text-o"></i>
                <span>'._t('Users').'</span>
            ',
                'url'=>array('/control/user/index'),
                'itemOptions'=>array('class'=>'treeview'),
            ),
            array(
                'label'=>'
                <i class="fa fa-file-text-o"></i>
                <span>'._t('Discounts').'</span>
            ',
                'url'=>array('/control/userCompanies/index'),
                'itemOptions'=>array('class'=>'treeview'),
            ),
        );
    }

    /**
     * Publish admin stylesheets,images,scripts,etc.. and return assets url
     *
     * @access public
     * @return string Assets url
     */
    public function getAssetsPath()
    {
        if($this->_assetsPath===null)
        {
            $this->_assetsPath = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('application.modules.'.$this->module->id.'.assets'),
                false,
                -1,
                YII_DEBUG
            );
        }
        return $this->_assetsPath;
    }

    /**
     * Set assets path
     *
     * @param string $path
     * @access public
     * @return void
     */
    public function setAssetsUrl($path)
    {
        $this->_assetsUrl = $path;
    }

    /**
     * @return array action filters
     */
    /*public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }*/

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('login'),
                'users' => array('*'),// для всех
            ),
            array('allow',
                'actions' => array('view', 'create', 'update', 'delete', 'index', 'logout','admin', 'upload', 'crop'),
                'roles' => array('admin'),// для авторизованных
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
}