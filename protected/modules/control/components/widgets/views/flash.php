<?php if(Yii::app()->user->hasFlash('error')): ?>
    <div class="alert alert-error alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4>
            <i class="icon fa fa-check"></i>
            <?= Yii::t('main', 'Error'); ?>
        </h4>
        <b><?= Yii::app()->user->getFlash('error'); ?></b>
        <?= Chtml::errorSummary($model); ?>
    </div>
<?php elseif (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4>
            <i class="icon fa fa-check"></i>
            <?= Yii::t('main', 'Success'); ?>
        </h4>
        <b><?= Yii::app()->user->getFlash('success'); ?></b>
    </div>
<?php endif; ?>