<?php

class CompanyController extends AdminController
{
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$company = new Company;
        $companyAddress = new CompanyAddress();
        $companyDescription = new CompanyDescription();

        if(isset($_POST['Company']))
		{
            $transaction = $company->dbConnection->beginTransaction();
            $company->attributes = $_POST['Company'];
            $companyDescription->attributes = $_POST['CompanyDescription'];
            $companyAddress->attributes = $_POST['CompanyAddress'];

            if($company->save()){
                $companyDescription->company_id = $company->id;
                $companyAddress->company_id = $company->id;

                if($companyDescription->save() && $companyAddress->save()) {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', Yii::t('main', 'Данные успешно сохранены!'));
                    $this->redirect(array('update','id'=>$company->id));
                } else {
                    $transaction->rollback();

                    Yii::app()->user->setFlash('error', Yii::t('main', 'Ошибка сохранения данных!'));
                }
            } else {
                $transaction->rollback();

                Yii::app()->user->setFlash('error', Yii::t('main', 'Ошибка сохранения данных!'));
            }
		}

		$this->render('create',array(
			'company' => $company,
            'companyDescription' => $companyDescription,
            'companyAddress' => $companyAddress,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $company = $this->loadModel($id);
        $companyDescription = CompanyDescription::model()->find('company_id = :id', array(':id'=>$company->id));
        $companyAddress = CompanyAddress::model()->find('company_id = :id', array(':id'=>$company->id));

		if(isset($_POST['Company']))
		{
            $transaction = $company->dbConnection->beginTransaction();
            $company->attributes = $_POST['Company'];
            $companyDescription->attributes = $_POST['CompanyDescription'];
            $companyAddress->attributes = $_POST['CompanyAddress'];

            if($company->save()){
                $companyDescription->company_id = $company->id;
                $companyAddress->company_id = $company->id;

                if($companyDescription->save() && $companyAddress->save()) {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', Yii::t('main', 'Данные успешно сохранены!'));
                    $this->refresh();
                } else {
                    $transaction->rollback();

                    Yii::app()->user->setFlash('error', Yii::t('main', 'Ошибка сохранения данных!'));
                }
            } else {
                $transaction->rollback();

                Yii::app()->user->setFlash('error', Yii::t('main', 'Ошибка сохранения данных!'));
            }
		}

		$this->render('update',array(
            'company' => $company,
            'companyDescription' => $companyDescription,
            'companyAddress' => $companyAddress,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model=new Company('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Company']))
            $model->attributes=$_GET['Company'];

        $this->render('index',array(
        'model'=>$model,
        ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Company the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Company::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;

	}

	/**
	 * Performs the AJAX validation.
	 * @param Company $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='company-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
