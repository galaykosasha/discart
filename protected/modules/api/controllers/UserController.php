<?php
class UserController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $request = json_decode(file_get_contents('php://input'));
        $answer = array();
        if(isset($request->action) && !empty($request->action)) {
            switch($request->action) {
                case 'addUser':
                    $result = $this->addUser($request->card_number, $request->telephone);
                    if($result['processed'])
                        $answer['success'] = $result['message'];
                    else
                        $answer['error'] = $result['message'];
                    break;

                case 'showRestaurants':
                    $queryResult = $this->showRestaurants($request->user_id);
                    if(is_array($queryResult))
                        $answer['success'] = $queryResult;
                    else
                        $answer['error'] = Yii::t('main', 'Error query');
                    break;

                case 'showHistoryPays':
                    $queryResult = $this->showHistoryPays($request['user_id']);
                    if(is_array($queryResult) && !empty($queryResult))
                        $answer['success'] = $queryResult;
                    else
                        $answer['error'] = Yii::t('main', 'Error query');
                    break;

                default:
                    $answer['error'] = Yii::t('main', 'Unknown request');
                    break;
            }
        }
        echo CJSON::encode($answer);
    }

    /**
     * Added card to user
     * @param $card_number
     * @param $telephone
     * @return array
     */
    private function addUser($card_number, $telephone)
    {
        $result = array('processed'=>false, 'message'=>Yii::t('main', 'Unknown error'));

        $card = Cards::model()->find(array('condition' => 'card_number = :number', 'params'=>array(':number'=>$card_number)));
        if($card->active) {
            $result['message'] = Yii::t('main', 'This card has owner');
            return $result;
        }

        if(isset($card) && !empty($card))
        {
            $user = new User();
            $user->telephone = $telephone;
            $user->card_id = $card->id;
            if($user->save()) {

                $card->active = 1;
                $card->save();

                $result['processed'] = true;
                $result['message'] = array('status'=>Yii::t('main', 'Card added success'),'user_id'=>$user->id);
                return $result;
            }
            else
                $result['message'] = implode(' ,', $user->getErrors());
        }
        return $result;
    }

    /**
     * @param $id
     * @return array|bool
     */
    private function showRestaurants($id)
    {
        $model = UserCompanies::model()->findAll(
            array(
                'select'=>'company_id, discount',
                'condition'=>'user_id = :id',
                'params'=>array(':id'=>(int)$id)
            )
        );

        if(empty($model) || !isset($model))
            return array('message'=>'У Вас нет доступных скидок');


        $prepareArray = array();

        foreach($model as $UserCompany)
        {
            $company = Company::model()->findByPk($UserCompany->company_id);
            $prepareArray[] = array(
                'discount' => $UserCompany->discount,
                'name_ru' => $company->companyDescriptions->name_ru,
                'description_ru' => $company->companyDescriptions->description_ru,
                'address' => $company->companyAddresses->country.' '.$company->companyAddresses->city.' '.$company->companyAddresses->street,
            );
        }
        return $prepareArray;
    }

    /**
     * @param $id
     * @return array|bool
     */
    private function showHistoryPays($id)
    {
        $model = HistoryPayd::model()->with('restorant')->findAll(
            array(
                'select'=>'restorant_id, date',
                'condition'=>'user_id = :id',
                'params'=>array(':id'=>$id)
            )
        );

        if(empty($model) || !isset($model))
            return false;

        $prepareArray = array();

        foreach($model as $restaurants)
        {
            $restaurant = Restorant::model()->findByPk($restaurants->restorant_id);
            $prepareArray[] = array(
                'date' => $restaurants->date,
                'name_ru' => $restaurant->name_ru,
                'description_ru' => $restaurant->description_ru,
            );
        }
        return $prepareArray;
    }
}