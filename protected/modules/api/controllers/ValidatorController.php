<?php
class ValidatorController extends Controller
{
    public function actionIndex()
    {
        $request = CJSON::decode(file_get_contents('php://input'));
        if(isset($request['query']) && !empty($request['query']))
        {
            switch($request['query'])
            {
                case 'card_number':
                    echo json_encode($this->validateCard($request['number']));
                    return;
                case 'telephone':
                    echo json_encode($this->validateTelephone($request['number']));
                    return;
            }
        }
        echo json_encode(array('error'=>true, 'message'=>Yii::t('main', 'Not correct request')));
    }

    public function validateCard($number)
    {
        $answer = array();
        $card = Cards::model()->find(array('condition'=>'card_number = :number', 'params'=>array(':number'=>$number)));

        if(!isset($card) || empty($card)) {
            $answer['error'] = true;
            $answer['message'] = Yii::t('main', 'Card not isset');
        } elseif(!$this->pregCardNumber((string)$number)) {
            $answer['error'] = true;
            $answer['message'] = Yii::t('main', 'Card number not correct');
        } elseif($card->active) {
            $answer['error'] = true;
            $answer['message'] = Yii::t('main', 'Card has owner');
        } else {
            $answer['success'] = true;
        }
        return $answer;
    }

    private function pregCardNumber($input_line)
    {
        preg_match("/\A\d{16}\z/", (string)$input_line, $output_array);
        return !empty($output_array) ? true : false;
    }

    private function validateTelephone($telephone)
    {
        $answer = array();
        $user = User::model()->find(array('condition'=>'telephone = :telephone', 'params'=>array(':telephone'=>$telephone)));
        if(isset($user) || !empty($user)) {
            $answer['error'] = true;
            $answer['message'] = Yii::t('main', 'Another user has this telephone number');
        } else {
            $answer['success'] = true;
        }
        return $answer;
    }
}