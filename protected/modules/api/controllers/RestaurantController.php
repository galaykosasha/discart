<?php
class RestaurantController extends Controller
{
    public function actionIndex()
    {
        $answer = array();
        if(isset($_POST)) {
            switch($_POST['action']) {
                case 'addCart':
                    $result = $this->addCart($_POST['cart_number'], $_POST['restaurant_id'], $_POST['discount']);
                    if($result['processed'])
                        $answer['success'] = $result['message'];
                    else
                        $answer['error'] = $result['message'];
                    break;

                case 'showDiscount':
                    $queryResult = $this->showDiscount($_POST['cart_number'], $_POST['restaurant_id']);
                    if(is_array($queryResult) && !empty($queryResult))
                        $answer['success'] = $queryResult;
                    else
                        $answer['error'] = Yii::t('main', 'Error query');
                    break;

                default:
                    $answer['error'] = Yii::t('main', 'Unknown request');
                    break;
            }
        }
        return CJSON::encode($answer);
    }

    private function addCart($cart_number, $restaurant_id, $discount)
    {
        $result = array('processed'=>false, 'message'=>Yii::t('main', 'Unknown error'));

        $user = $this->getUserByCartNumber($cart_number);

        if(!$user['processed']) {
            $result['message'] = $user['message'];
            return $result;
        }

        /* Register user in restaurant */
        $userRestaurant = new UserRestorant();
        $userRestaurant->restorant_id = $restaurant_id;
        $userRestaurant->user_id = $user['user']->id;
        $userRestaurant->discount = $discount;
        if($userRestaurant->save()) {
            $historyDiscount = new HistoryDiscount();
            $historyDiscount->user_id = $user['user']->id;
            $historyDiscount->date = new CDbExpression("NOW()");
            $historyDiscount->restorant_login = $userRestaurant->restorant->login;
            $historyDiscount->save();
            $result['processed'] = true;
            $result['message'] = Yii::t('main', 'Cart added success');
        }else
            $result['message'] = implode(' ,', $userRestaurant->getErrors());
        return $result;
    }

    private function showDiscount($cart_number, $restaurant_id)
    {
        $result = array('processed'=>false, 'message'=>Yii::t('main', 'Unknown error'));

        $user = $this->getUserByCartNumber($cart_number);

        if(!$user['processed']) {
            $result['message'] = $user['message'];
            return $result;
        }

        $userRestaurant = UserRestorant::model()->find(
            array(
                'condition'=>'user_id = :user_id AND restorant_id = :restorant_id',
                'params' => array(':user_id' => $user['user']->id, ':restorant_id' => $restaurant_id),
            )
        );

        if(!isset($userRestaurant) || empty($userRestaurant)) {
            $result['message'] = Yii::t('main', 'This user not have discount in this restaurant');
        } else {
            $result['processed'] = true;
            $result['message'] = $userRestaurant->discount;
        }
        return $result;
    }

    /**
     * Returned array width User::model or error
     * @param $cart_number
     * @return array
     */
    private function getUserByCartNumber($cart_number)
    {
        $result = array('processed'=>false, 'message'=>Yii::t('main', 'Unknown error'));

        $cart = Carts::model()->find(array('condition'=>'cart_number = :number', 'params'=>array(':number'=>$cart_number)));

        if(!isset($cart) || empty($cart)) {
            $result['message'] = Yii::t('main', 'This cart not isset');
            return $result;
        }elseif(!$cart->active) {
            $result['message'] = Yii::t('main', 'This cart has owner');
            return $result;
        }

        $user = User::model()->find(array('condition'=>'cart_id = :cart_id', 'params'=>array(':cart_id'=>$cart->id)));

        if(!isset($user) || empty($user)) {
            $result['message'] = Yii::t('main', 'User not find! Maybe any user not have this cart');
            return $result;
        }
        $result = array('processed'=>true, 'message'=>Yii::t('main', 'Ok!'), 'user'=>$user);

        return $result;
    }
}