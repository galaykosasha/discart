<?php
class HelperCompany extends Company
{
    public $country;
    /**
     * @param string $className
     * @return Company
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}