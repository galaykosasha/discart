<?php

/**
 * This is the model class for table "{{company_description}}".
 *
 * The followings are the available columns in table '{{company_description}}':
 * @property string $id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_uk
 * @property string $description_en
 * @property string $description_ru
 * @property string $description_uk
 * @property string $phones
 * @property string $company_id
 *
 * The followings are the available model relations:
 * @property Company $company
 */
class CompanyDescription extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{company_description}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name_en, name_ru, name_uk, company_id', 'required'),
			array('name_en, name_ru, name_uk, phones', 'length', 'max'=>255),
			array('company_id', 'length', 'max'=>11),
			array('description_en, description_ru, description_uk', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_en, name_ru, name_uk, description_en, description_ru, description_uk, phones, company_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'name_en' => Yii::t('main', 'Name En'),
			'name_ru' => Yii::t('main', 'Name Ru'),
			'name_uk' => Yii::t('main', 'Name Uk'),
			'description_en' => Yii::t('main', 'Description En'),
			'description_ru' => Yii::t('main', 'Description Ru'),
			'description_uk' => Yii::t('main', 'Description Uk'),
			'phones' => Yii::t('main', 'Phones'),
			'company_id' => Yii::t('main', 'Company'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name_en',$this->name_en,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('name_uk',$this->name_uk,true);
		$criteria->compare('description_en',$this->description_en,true);
		$criteria->compare('description_ru',$this->description_ru,true);
		$criteria->compare('description_uk',$this->description_uk,true);
		$criteria->compare('phones',$this->phones,true);
		$criteria->compare('company_id',$this->company_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyDescription the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
