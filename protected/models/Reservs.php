<?php

/**
 * This is the model class for table "{{reservs}}".
 *
 * The followings are the available columns in table '{{reservs}}':
 * @property integer $id
 * @property integer $restorant_id
 * @property integer $user_id
 * @property integer $peoples_count
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Restorant $restorant
 * @property User $user
 */
class Reservs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{reservs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('restorant_id, user_id, peoples_count', 'required'),
			array('restorant_id, user_id, peoples_count', 'numerical', 'integerOnly'=>true),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, restorant_id, user_id, peoples_count, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'restorant' => array(self::BELONGS_TO, 'Restorant', 'restorant_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'restorant_id' => Yii::t('main', 'Restorant'),
			'user_id' => Yii::t('main', 'User'),
			'peoples_count' => Yii::t('main', 'Peoples Count'),
			'date' => Yii::t('main', 'Date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('restorant_id',$this->restorant_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('peoples_count',$this->peoples_count);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reservs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
