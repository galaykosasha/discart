<?php

/**
 * This is the model class for table "{{history_payd}}".
 *
 * The followings are the available columns in table '{{history_payd}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $restorant_id
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Restorant $restorant
 * @property User $user
 */
class HistoryPayd extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{history_payd}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id, restorant_id', 'required'),
			array('user_id, restorant_id', 'numerical', 'integerOnly'=>true),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, restorant_id, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'restorant' => array(self::BELONGS_TO, 'Restorant', 'restorant_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'user_id' => Yii::t('main', 'User'),
			'restorant_id' => Yii::t('main', 'Restorant'),
			'date' => Yii::t('main', 'Date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('restorant_id',$this->restorant_id);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HistoryPayd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
