<?php

/**
 * This is the model class for table "{{user_companies}}".
 *
 * The followings are the available columns in table '{{user_companies}}':
 * @property string $id
 * @property string $discount
 * @property string $user_id
 * @property string $company_id
 * @property integer $company_chain_id
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserCompanies extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_companies}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('discount, user_id', 'required'),
			array('company_chain_id', 'numerical', 'integerOnly'=>true),
			array('discount', 'length', 'max'=>2),
			array('user_id, company_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, discount, user_id, company_id, company_chain_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'discount' => Yii::t('main', 'Discount'),
			'user_id' => Yii::t('main', 'User'),
			'company_id' => Yii::t('main', 'Company'),
			'company_chain_id' => Yii::t('main', 'Company Chain'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('discount',$this->discount,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('company_id',$this->company_id,true);
		$criteria->compare('company_chain_id',$this->company_chain_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserCompanies the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
