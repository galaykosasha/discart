<?php

/**
 * This is the model class for table "{{reserves}}".
 *
 * The followings are the available columns in table '{{reserves}}':
 * @property string $id
 * @property string $peoples_count
 * @property string $date
 * @property string $user_id
 * @property string $company_id
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property User $user
 */
class Reserves extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{reserves}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('peoples_count, user_id, company_id', 'required'),
			array('peoples_count, user_id, company_id', 'length', 'max'=>11),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, peoples_count, date, user_id, company_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'peoples_count' => Yii::t('main', 'Peoples Count'),
			'date' => Yii::t('main', 'Date'),
			'user_id' => Yii::t('main', 'User'),
			'company_id' => Yii::t('main', 'Company'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('peoples_count',$this->peoples_count,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('company_id',$this->company_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reserves the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
