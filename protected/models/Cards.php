<?php

/**
 * This is the model class for table "{{cards}}".
 *
 * The followings are the available columns in table '{{cards}}':
 * @property string $id
 * @property string $card_number
 * @property integer $active
 * @property integer $status
 * @property integer $company_id
 *
 * The followings are the available model relations:
 * @property User[] $users
 */
class Cards extends CActiveRecord
{
    const CARD_NOT_ACTIVE = 0;
    const CARD_ACTIVE = 1;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cards}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('card_number', 'required'),
			array('active, status, company_id', 'numerical', 'integerOnly'=>true),
			array('card_number', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, card_number, active, status, company_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'users' => array(self::HAS_MANY, 'User', 'card_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'card_number' => Yii::t('main', 'Card Number'),
			'active' => Yii::t('main', 'Active'),
			'status' => Yii::t('main', 'Status'),
			'company_id' => Yii::t('main', 'Company'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->condition = 'active = :not_active';
        $criteria->params = array(':not_active'=>self::CARD_NOT_ACTIVE);

		$criteria->compare('id',$this->id,true);
		$criteria->compare('card_number',$this->card_number,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('status',$this->status);
		$criteria->compare('company_id',$this->company_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cards the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
