<?php

/**
 * This is the model class for table "{{restorant}}".
 *
 * The followings are the available columns in table '{{restorant}}':
 * @property integer $id
 * @property integer $login
 * @property string $password
 * @property string $name_ru
 * @property string $description_ru
 * @property string $address
 *
 * The followings are the available model relations:
 * @property HistoryDiscount[] $historyDiscounts
 * @property HistoryPayd[] $historyPayds
 * @property Reservs[] $reservs
 * @property UserRestorant[] $userRestorants
 */
class Restorant extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{restorant}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('login, password, name_ru, address', 'required'),
			array('login', 'numerical', 'integerOnly'=>true),
			array('password, name_ru, address', 'length', 'max'=>255),
			array('description_ru', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, login, password, name_ru, description_ru, address', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'historyDiscounts' => array(self::HAS_MANY, 'HistoryDiscount', 'restorant_login'),
			'historyPayds' => array(self::HAS_MANY, 'HistoryPayd', 'restorant_id'),
			'reservs' => array(self::HAS_MANY, 'Reservs', 'restorant_id'),
			'userRestorants' => array(self::HAS_MANY, 'UserRestorant', 'restorant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'login' => Yii::t('main', 'Login'),
			'password' => Yii::t('main', 'Password'),
			'name_ru' => Yii::t('main', 'Name Ru'),
			'description_ru' => Yii::t('main', 'Description Ru'),
			'address' => Yii::t('main', 'Address'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('description_ru',$this->description_ru,true);
		$criteria->compare('address',$this->address,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Restorant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
