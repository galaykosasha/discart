<?php

/**
 * This is the model class for table "{{carts}}".
 *
 * The followings are the available columns in table '{{carts}}':
 * @property integer $id
 * @property string $cart_number
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property User[] $users
 */
class Carts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{carts}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('cart_number', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('cart_number', 'length', 'max'=>16),
			array('cart_number', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cart_number, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'users' => array(self::HAS_MANY, 'User', 'cart_id'),
		);
	}

    public function getNum()
    {
        return '\''.$this->cart_number;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'cart_number' => Yii::t('main', 'Cart Number'),
			'active' => Yii::t('main', 'Active'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cart_number',$this->cart_number,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Carts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
