<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property string $id
 * @property string $telephone
 * @property integer $sex
 * @property string $card_id
 *
 * The followings are the available model relations:
 * @property HistoryDiscount[] $historyDiscounts
 * @property HistoryPay[] $historyPays
 * @property Reserves[] $reserves
 * @property Cards $card
 * @property UserCompanies[] $userCompanies
 * @property UserDevices[] $userDevices
 * @property UserSocial[] $userSocials
 */
class User extends CActiveRecord
{

    const SEX_FEMALE = 0;
    const SEX_MALE = 1;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('card_id, telephone', 'required'),
			array('sex', 'numerical', 'integerOnly'=>true),
			array('telephone', 'length', 'max'=>20),
			array('card_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, telephone, sex, card_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'historyDiscounts' => array(self::HAS_MANY, 'HistoryDiscount', 'user_id'),
			'historyPays' => array(self::HAS_MANY, 'HistoryPay', 'user_id'),
			'reserves' => array(self::HAS_MANY, 'Reserves', 'user_id'),
			'card' => array(self::BELONGS_TO, 'Cards', 'card_id'),
			'userCompanies' => array(self::HAS_MANY, 'UserCompanies', 'user_id'),
			'userDevices' => array(self::HAS_MANY, 'UserDevices', 'user_id'),
			'userSocials' => array(self::HAS_MANY, 'UserSocial', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'telephone' => Yii::t('main', 'Telephone'),
			'sex' => Yii::t('main', 'Sex'),
			'card_id' => Yii::t('main', 'Card'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('card_id',$this->card_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
