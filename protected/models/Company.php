<?php

/**
 * This is the model class for table "{{company}}".
 *
 * The followings are the available columns in table '{{company}}':
 * @property string $id
 * @property string $password
 * @property string $telephone
 * @property integer $type
 * @property integer $segment
 * @property string $company_chain_id
 *
 * The followings are the available model relations:
 * @property CompanyChains $companyChain
 * @property CompanyAddress[] $companyAddresses
 * @property CompanyDescription[] $companyDescriptions
 * @property CompanyDevices[] $companyDevices
 * @property HistoryDiscount[] $historyDiscounts
 * @property HistoryPay[] $historyPays
 * @property Reserves[] $reserves
 */
class Company extends CActiveRecord
{
    const TYPE_RESTAURANT = 1;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{company}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('password, telephone, type, segment', 'required'),
			array('type, segment', 'numerical', 'integerOnly'=>true),
			array('password, telephone', 'length', 'max'=>255),
			array('company_chain_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, password, telephone, type, segment, company_chain_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'companyChain' => array(self::BELONGS_TO, 'CompanyChains', 'company_chain_id'),
			'companyAddresses' => array(self::HAS_MANY, 'CompanyAddress', 'company_id'),
			'companyDescriptions' => array(self::HAS_ONE, 'CompanyDescription', 'company_id'),
			'companyDevices' => array(self::HAS_MANY, 'CompanyDevices', 'company_id'),
			'historyDiscounts' => array(self::HAS_MANY, 'HistoryDiscount', 'company_id'),
			'historyPays' => array(self::HAS_MANY, 'HistoryPay', 'company_id'),
			'reserves' => array(self::HAS_MANY, 'Reserves', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main', 'ID'),
			'password' => Yii::t('main', 'Password'),
			'telephone' => Yii::t('main', 'Telephone'),
			'type' => Yii::t('main', 'Type'),
			'segment' => Yii::t('main', 'Segment'),
			'company_chain_id' => Yii::t('main', 'Company Chain'),
		);
	}

    public function getName_ru()
    {
        return $this->companyDescriptions->name_ru;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('segment',$this->segment);
		$criteria->compare('company_chain_id',$this->company_chain_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
