<?php
return array (
  'template' => 'admin',
  'connectionId' => 'db',
  'tablePrefix' => 'dsc_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '0',
);
